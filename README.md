# AI War Customizable AI Type Randomizer

**Author:** Robert Grady &lt;[r2b2grady@gmail.com](mailto:r2b2grady@gmail.com)&gt;

## TODO

* Incorporate logic to skip various exclusion sets if there are no preceding
  inclusion sets specified (e.g. if `include.types` is the only inclusion and
  `exclude.difficulty` is set).

## About

This tool allows you to select two random AI Types for Arcengames' game
_AI War: Fleet Command_ while excluding certain types or combinations.

It does _NOT_ run inside _AI War_, but instead runs in the command line to
generate one or more pairs of AI Types and prints them out. It's up to you to
then select them in _AI War_.

### A Note on Paths

All paths in this document are listed in Unix format&mdash;i.e. with forward
slashes instead of backslashes. All non-absolute paths are relative to the root
`AI War Randomizer` directory, unless otherwise noted.

## Installation

To install the program, simply download the files and extract the root
`AI War Randomizer` directory onto your file system.

**Note:** This program has an exclusively command-line interface, so if you want
to be run it from a command window without having to type out the entire path
every time, make sure that the `AI War Randomizer/src` and/or
`AI War Randomizer/bin` paths are in your `PATH` environment variable.

## Configuring the Program

The program needs some initial setup to get the most out of it. You can run it
immediately, but you'll only be able to specify the same presets as _AI War_
itself, since it doesn't come with any presets.

To add your own presets, add new JSON files in the `config/custom/` directory
(see the [Custom Presets](#custom-presets) section below for details on the
schema).

## Running the Program

To run the program, simply call it from the command line with the name of the
preset you want to use. For example, if I have a preset called `r2b2-default`
that I want to use, I would simply run one of the following commands:

```
    $ aiw-random.pl r2b2-default
    $ aiw-random r2b2-default
```

For more details on the other command-line options, use the `-?`, `-h`, or
`--help` command-line option.

## Custom Presets

Custom presets are stored in one or more JSON files. These are located in the
custom JSON directories, configured by the `$.dirs.custom[*]` member of the file
`config/aiwrand_cfg.json`. By default, this is only the `config/custom/`
directory, but you can add more directories to that list.

Each JSON file must have an object as its root element. Each of the object's
key-value pairs represents a preset&mdash;the key is the name used to call the
preset from the command line, while the value is a **Preset Object**&mdash;a special
object representing the preset and defined by the following schema.

### Preset Object Schema

Each Preset Object supports the following fields:

| Field Name | Required | Contains                                             |
| ---------- | -------- | ---------------------------------------------------- |
| `name`     | Yes      | The human-readable name of the preset.               |
| `include`  | Partial  | An **AI Type Set** object (see [AI Type Set Objects](#ai-type-set-objects) section) describing which AI Types to include. This **or** the `exclude` key must be present. |
| `exclude`  | Partial  | An AI Type Set object describing which AI Types to exclude. This **or** the `include` key must be present. |
| `pairs`    | No       | A list of AI Type pairs to exclude from consideration. This must be a list of lists, where each sub-list is composed of two AI keys. |

If there is no `include` key present in a preset, then it is treated the same as
if it was present and configured to include all AI Types (i.e. the `exclude`
operations will be performed on the set of all AI Types).

#### AI Type Set Objects

The `include` and `exclude` keys are both AI Type Set objects&mdash;objects that
represent sets of AI Types for the set operations that the script performs
before randomly selecting two AI Types.

The following keys are supported and at least one must be present for an AI Type
Set object to be valid:

| Field Name   | Contains                                                      |
| ------------ | ------------------------------------------------------------- |
| `difficulty` | A list of AI difficulty set keys (`e`/`m`/`h`/`t` for Easier/Moderate/Harder/Technologist, resp.). |
| `expansions` | A list of AI War expansion set keys (see the [AI War Expansions](#ai-war-expansions) section below). |
| `types`      | A list of AI Type keys (see the [AI Types](#ai-types) section below). |

##### Set Logic

These `include` and `exclude` sets are combined using the following algorithm:

1. Add all AI Types described by `include.difficulties` to the list of types
   (hereafter simply List).
2. Remove all AI Types described by `exclude.difficulties` from the List.
3. Add all AI Types described by `include.expansions` to the List.
4. Remove all AI Types described by `exclude.expansions` from the List.
5. Add all AI Types listed in `include.types` to the List.
6. Remove all AI Types listed in `exclude.types` from the List.

If one or more sub-keys of `include` are not present, the script will go down
this list and start at the first one that exists. E.g. if `exclude.difficulties`
is defined but `include.difficulties` is not, `exclude.difficulties` will be
ignored.

### Lists of Keys

This section contains lists of keys used for designing custom presets.

#### AI War Expansions

| Key   | Expansion                |
| ----- | ------------------------ |
| `aiw` | Base                     |
| `zr`  | Zenith Remnant           |
| `cn`  | Children of Neinzul      |
| `ls`  | Light of the Spire       |
| `as`  | Ancient Shadows          |
| `vm`  | Vengeance of the Machine |
| `dw`  | Destroyer of Worlds      |

#### AI Types

| Key | Type Name | Difficulty | Expansion |
| --- | --------- | ---------- | --------- |
| `bouncer` | Bouncer | Easier | Vengeance of the Machine |
| `chivalric` | Chivalric | Easier | Vengeance of the Machine |
| `cowardly` | Cowardly | Easier | Destroyer of Worlds |
| `homeworld` | Entrenched Homeworlder | Easier | Base |
| `fort-baron` | Fortress Baron | Easier | Base |
| `grav-drill` | Grav Driller | Easier | Zenith Remnant |
| `mines` | Mine Enthusiast | Easier | Base |
| `n-viral` | Neinzul Viral Enthusiast | Easier | Children of Neinzul |
| `ninny` | Shield Ninny | Easier | Zenith Remnant |
| `hammer` | Sledge Hammer | Easier | Base |
| `support` | Support Corps | Easier | Children of Neinzul |
| `tank` | The Tank | Easier | Base |
| `trains` | Train Master | Easier | Base |
| `turtle` | Turtle | Easier | Base |
| `vanilla` | Vanilla | Easier | Light of the Spire |
| `artill` | Artillerist | Moderate | Vengeance of the Machine |
| `assassin` | Assassin | Moderate | Base |
| `backdoor` | Backdoor Hacker | Moderate | Base |
| `bully` | Bully | Moderate | Base |
| `camo` | Camouflager | Moderate | Zenith Remnant |
| `ctr-spy` | Counter Spy | Moderate | Base |
| `exotic` | Exotic | Moderate | Vengeance of the Machine |
| `experiment` | Experimentalist | Moderate | Zenith Remnant |
| `parasite` | Feeding Parasite | Moderate | Base |
| `kite` | Kite Flier | Moderate | Destroyer of Worlds |
| `bomber` | Mad Bomber | Moderate | Base |
| `n-cluster` | Neinzul Cluster-Bomber | Moderate | Children of Neinzul |
| `youngster` | Neinzul Youngster | Moderate | Children of Neinzul |
| `oneway` | One-Way Doormaster | Moderate | Zenith Remnant |
| `peace` | Peacemaker | Moderate | Zenith Remnant |
| `reservist` | Reservist | Moderate | Vengeance of the Machine |
| `racer` | Speed Racer | Moderate | Zenith Remnant |
| `spire` | Spireling | Moderate | Light of the Spire |
| `stealth` | Stealth Master | Moderate | Base |
| `tagteam` | Tag Teamer | Moderate | Zenith Remnant |
| `teleturtle` | Teleporter Turtle | Moderate | Base |
| `thief` | Thief | Moderate | Light of the Spire |
| `vic-raider` | Vicious Raider | Moderate | Base |
| `zenith` | Zenith Descendant | Moderate | Zenith Remnant |
| `alarmist` | Alarmist | Harder | Zenith Remnant |
| `attrition` | Attritioner | Harder | Base |
| `everything` | Everything | Harder | Light of the Spire |
| `ext-raider` | Extreme Raider | Harder | Light of the Spire |
| `fort-king` | Fortress King | Harder | Ancient Shadows |
| `golemite` | Golemite | Harder | Zenith Remnant |
| `mime` | Mime | Harder | Destroyer of Worlds |
| `n-nester` | Neinzul Nester | Harder | Children of Neinzul |
| `overreactive` | Overreactive | Harder | Destroyer of Worlds |
| `quadratic` | Quadratic | Harder | Destroyer of Worlds |
| `jammer` | Radar Jammer | Harder | Zenith Remnant |
| `retaliatory` | Retaliatory | Harder | Light of the Spire |
| `scorch` | Scorched Earth | Harder | Base |
| `shadow` | Shadow Master | Harder | Base |
| `specops` | Special Forces Captain | Harder | Base |
| `sp-hammer` | Spire Hammer | Harder | Light of the Spire |
| `starfleet` | Starfleet Commander | Harder | Zenith Remnant |
| `starship` | Starship Fanatic | Harder | Destroyer of Worlds |
| `vanguard` | Vanguard | Harder | Destroyer of Worlds |
| `vengeful` | Vengeful | Harder | Vengeance of the Machine |
| `vorpal` | Vorpal | Harder | Vengeance of the Machine |
| `warp` | Warp Jumper | Harder | Children of Neinzul |
| `brutal` | Brutal | Technologist | Destroyer of Worlds |
| `c-spire` | Crafty Spire | Technologist | Light of the Spire |
| `heroic` | Heroic | Technologist | Ancient Shadows |
| `r-engine` | Raid Engine | Technologist | Zenith Remnant |
| `tech-homeworlder` | Technologist Homeworlder | Technologist | Base |
| `tech-parasite` | Technologist Parasite | Technologist | Base |
| `tech-raider` | Technologist Raider | Technologist | Base |
| `tech-sledge` | Technologist Sledge | Technologist | Base |
| `tech-turtle` | Technologist Turtle | Technologist | Base |
| `core` | The Core | Technologist | Base |
| `vic-exotic` | Vicious Exotic | Technologist | Base |