#!/usr/bin/perl

use v5.18;

use Cwd qw(getcwd abs_path);
use Data::Dumper;
use File::Path qw(mkpath);
use File::Spec::Functions qw(rel2abs);
use File::Slurper qw(read_text);
use JSON;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
use Pod::Usage;

my $rundir = rel2abs($0 =~ s#\\#/#gr =~ s#/[^/]*$##r);
$rundir =~ s#/src/*$##;

$Data::Dumper::Indent = 1;
$Data::Dumper::Sortkeys = 1;

say STDERR $rundir;

our $NAME = "AI War AI Type Randomizer";
our $VERSION = '1.0.0';

=head1 SYNOPSIS

$ aiw-random SETNAME [-c|--compile]

$ aiw-random [(-e|--exclude) TYPES] [(-i|--include) TYPES] [(-x|--exclude-pairs)
COMBOS]

$ aiw-random [-?|-h|--help] [-o|--options] [-V|--version]

=head1 DESCRIPTION

Generates one or more random pairs of AI Types for I<AI War: Fleet Command>
while filtering out specific types. Custom-named presets can be stored in one or
more JSON files as configured in the B<config/custom.json> file.

Parameters can also be specified manually with the command-line options.

=head1 OPTIONS AND ARGUMENTS

=over 8

=item I<SETNAME>

The name of a preset configuration from the default file C<config/aiw_data.json>
or any of the custom configurations in the custom configuration directories set
up in the C<config/custom.json> file (defaults to C<config/custom/*.json>).

=item B<-e> I<TYPES>, B<--exclude> I<TYPES>

Exclude each of the types, difficulties, or expansions listed in the
semicolon-separated list I<TYPES>. The difficulties, expansions, and types can
be referred to by any name in their B<key>, B<name>, or (for difficulties)
B<aliases> attributes from the C<config/aiw_data.json>.

For example, the B<Technologist Parasite> can be referred to as "tech-parasite"
or "Technologist Parasite". All B<Easier> AI types could be referred to as
"Easier", "easier", or "e".

=item B<-i> I<TYPES>, B<--include> I<TYPES>

Include each of the types/difficulties/expansions listed in the
semicolon-separated list I<TYPES>. These can be any of the same sorts of codes
as taken by the B<-e>/B<--exclude> flag shown above.

=item B<-x> I<COMBOS>, B<--exclude-pairs> I<COMBOS>

Exclude each of the I<pairs> in the semicolon-separated list I<COMBOS>. Each
pair is composed of two AI Type expressions separated by a colon. Each
expression can be:

=over 8

=item 1

A single AI Type name or key (e.g. "Stealth Master" or "stealth", respectively,
for the B<Stealth Master> AI Type).

=item 2

A set of AI Type names/keys, separated by commas and enclosed in square
brackets. For example, the expression C<"[alarmist,Vicious
Raider,Thief,golemite,Mime]"> would indicate the B<Alarmist>, B<Vicious Raider>,
B<Thief>, B<Golemite>, and B<Mime> AI Types.

=back

For example, to prevent the randomizer from giving you any combination of the AI
Types B<Fortress King>, B<Shield Ninny>, B<Turtle>, B<Teleporter Turtle>,
B<Shadow Master>, and B<Radar Jammer>, use the expression
C<"[fort-king,ninny,teleturtle,shadow,jammer]:[fort-king,ninny,teleturtle,shadow,jammer]">.

This flag can be specified multiple times, with each subsequent use adding to
the list.

=item B<-c>, B<--compile>

Compile the preset I<SETNAME> into a single JSON file in the C<compiled>
directory. This allows the set to be loaded much more quickly.

=item B<-h>, B<--help>

Print this help text and exit.

=item B<-o>, B<--options>

Print this list of options and exit.

=item B<-V>, B<--version>

Print the version info and exit.

=back

=cut

my %opts = (exc_type    => [],
            inc_type    => [],
            exc_pair    => [],
            compile     => 0);

GetOptions('exclude|e=s@'       => \$opts{exc_type},
           'include|i=s@'       => \$opts{inc_type},
           'exclude-pairs|x=s@' => \$opts{exc_pair},
           'compile|c'          => \$opts{compile},
           'help|h|?'           => sub { pod2usage(-verbose => 2) },
           'options|o'          => sub { pod2usage(-verbose => 1) },
           'version|V'          => sub { say "$NAME v$VERSION"; exit(2) });

my $sname = shift;

pod2usage unless ($sname || @{$opts{exc_type}} || @{$opts{inc_type}});

# Look in $rundir/:$rundir/../:$rundir/../config/:$rundir/config/ for the
my ($cfg, $data);

my @srchdir = map { "$rundir/$_" } ('', qw(.. config ../config));

{
    my @fsrch = grep { -f $_ } map { "$_/aiw_data.json" } @srchdir;
    
    die "Cannot find dataset file (aiw_data.json) in " .
        join(", ", map { "$_/" } @srchdir)
        unless @fsrch;
    
    $data = load_data($fsrch[0]);
    
    @fsrch = grep { -f $_ } map { "$_/aiwrand_cfg.json" } @srchdir;
    
    die "Cannot find configuration file (aiwrand_cfg.json) in " .
        join(", ", map { "$_/" } @srchdir)
        unless @fsrch;
    
    $cfg = load_conf($fsrch[0]);
}

# TODO Check if there are any compiled files.

# Simple hashref representation of a set of AI Types. The 'types' key contains a
# list of allowed types, while the 'combos' key contains a list of forbidden
# combinations, represented as colon-separated two-element lists
# (e.g. "bouncer:chivalric").
my $set;

# Build the set's configuration or, if there's a compiled version of the set AND
# we aren't in compile mode, load it from the compiled file.
if (-f "$$cfg{compiled}/$sname.rand" && !$opts{compile}) {
    $set = load_comp("$$cfg{compiled}/$sname.rand");
} else {
    my $cust = load_custom($$cfg{dirs}{custom});
    
    say Dumper($cust);
    
    # "Compile" the preset in memory.
    my $p = $$cust{$sname} ||
        die "Cannot find a preset with the name $sname";
    
    # If there's nothing under 'include', that's taken to mean 'all'.
    $$p{include} = {difficulty => ['all']}
        unless exists $$p{include};
    
    # Hash of types where the value is a boolean indicating whether the type
    # will be included.
    my %types = map { ($_ => 0) } keys %{$$data{types}};
    
    # Parse $.include.difficulty
    if (exists $$p{include}{difficulty}) {
        my @idiff = map { ( $$data{difficulties}{$_} ?
                            $_ :
                            @{$$data{difficulty_sets}{$_}{include}{difficulty}} ) }
                    @{$$p{include}{difficulty}};
        
        map { my $d = $$data{types}{$_}{difficulty};
              $types{$_} = scalar(grep { $d eq $_ } @idiff) } keys %types;
    }

    # Parse $.exclude.difficulty
    if (exists $$p{exclude}{difficulty}) {
        my @xdiff = map { ( $$data{difficulties}{$_} ?
                            $_ :
                            @{$$data{difficulty_sets}{$_}{include}{difficulty}} ) }
                    @{$$p{exclude}{difficulty}};
        
        map { $types{$_} = 0 }
            grep { my $d = $$data{types}{$_}{difficulty};
                   grep { $_ eq $d } @xdiff; } keys %types;
    }
    
    # map { my $d = $_;
    #       map { $types{$_} = 1 }
    #           grep { fc $$data{types}{$_}{difficulty} eq fc $d } keys %{$$data{types}}; }
    #     map { ( $$data{difficulties}{$_} ?
    #             $_ :
    #             @{$$data{difficulty_sets}{$_}{include}{difficulty}} ) } @{$$p{include}{difficulty}};
    
    say Dumper(\%types);
    
    # TODO Manually parse the configuration.
}



############################### INTERNAL METHODS ###############################

#     load_comp($fcomp)
#
# Load the compiled preset from file $fcomp and return it as a hashref.
sub load_comp($) {
    my $f = shift;
    
    my $o = from_json(read_text($f));
    
    die "Compiled preset file $f is malformed"
        unless valid_preset($o);
    
    return $o;
}

#     load_conf($fconf)
#
# Load in the configuration from the JSON file $fconf. This is where the
# compiled file directory and the list of custom configuration directories are
# stored.
sub load_conf($) {
    my $f = shift;
    
    my $conf = from_json(read_text($f));
    
    # Expected results of ref() for each member of the 'dirs' element.
    my %reftypes = (compiled    => '',
                    custom      => 'ARRAY');
    
    # Verify the data.
    unless (exists $$conf{dirs}) {
        say STDERR ("Basic configuration file $f must have the element " .
                    "\"\$.dirs\".");
        
        exit 1;
    }
    
    my @missing = grep { !exists $$conf{dirs}{$_} } (qw(compiled custom));
    
    die sprintf("Could not find keys %s in configuration file %s: the " .
                "file might be corrupted or incomplete. Try " .
                "re-downloading it from %s or consulting the " .
                "documentation",
                join(', ', @missing),
                $f,
                'https://gitlab.com/r2b2grady/aiw-custom-randomizer/')
        if @missing;
    
    # String containing a semicolon-separated list of $$conf{dirs} keys
    # that have invalid types. Each element of the list is a three-element
    # comma-separated list in the format "$KEY,$EXPECTED,$ACTUAL", where $KEY is
    # the key, $EXPECTED is the expected type ('SCALAR' if $reftypes{$KEY} is
    # "", $reftypes{$KEY} otherwise), and $ACTUAL is the actual type.
    my $etype = join(";",
                     map { join(",",
                                $_,
                                ($reftypes{$_} || 'SCALAR'),
                                (ref($$conf{dirs}{$_}) || 'SCALAR')) }
                     grep { $reftypes{$_} ne ref($$conf{dirs}{$_}) }
                     sort keys %reftypes);
    
    die sprintf("The configuration file %s has one or more errors:\n%s",
                $f,
                join("\n",
                     map { sprintf("    %s: Expected %s, encountered %s",
                                   split /,/, $_); }
                     split(/;/, $etype)))
        if $etype;
    
    # Enforce forward slash as a path separator.
    $$conf{compiled} =~ s#\\#/#g;
    map { s#\\#/#g } @{$$conf{custom}};
    
    # Convert relative paths to absolute paths.
    $$conf{dirs}{compiled} = rel2abs($$conf{dirs}{compiled}, $rundir) =~ s#/+$##r;
    @{$$conf{dirs}{custom}} = map { rel2abs($_, $rundir) =~ s#/+$##r } @{$$conf{dirs}{custom}};
    
    return $conf;
}

#     load_custom($dirs)
#
# Load all custom set configuration files from the arrayref of directory paths
# $dirs.
sub load_custom($) {
    my $dirs = shift;
    
    # TODO Incorporate the file name into the loaded dataset for debugging?
    
    die "load_custom() requires an arrayref" unless ref($dirs) eq 'ARRAY';
    
    my $out = {};
    
    for my $d (@$dirs) {
        $d =~ s#\\#/#g;
        
        $d =~ s#/+$##;
        
        opendir my $dh, $d or die "Error opening directory $d: $!";
        
        for my $f (grep { !/^\./ && /\.json$/ } readdir $dh) {
            my $c = from_json(read_text("$d/$f"));
            
            die "The root element of the JSON file $d/$f is not an object"
                unless ref($c) eq 'HASH';
            
            for my $k (sort keys %$c) {
                say STDERR "$k: " . Dumper($$c{$k}) =~ s/^\$VAR\d+ = //r;
                
                if (exists $$out{$k}) {
                    # TODO Give indication of which other files have the
                    # duplicate key(s)?
                    die "Duplicate key $k found in file $d/$f";
                } else {
                    die "Configuration with key $k from file $d/$f is " .
                        "missing the 'name' field"
                        unless exists $$c{$k}{name};
                    
                    die "Configuration with key $k from file $d/$f does " .
                        "not have any inclusion or exclusion criteria"
                        unless ( ( exists($$c{$k}{include}) &&
                                   ref($$c{$k}{include}) eq 'HASH' &&
                                   %{$$c{$k}{include}}) ||
                                 ( exists($$c{$k}{exclude}) &&
                                   ref($$c{$k}{exclude}) eq 'HASH' &&
                                   %{$$c{$k}{exclude}} ) );
                    
                    die "Configuration $k is malformed"
                        unless (grep { $_ eq 'difficulty' ||
                                       $_ eq 'expansions' ||
                                       $_ eq 'types' }
                                sort (keys %{$$c{$k}{include}},
                                      keys %{$$c{$k}{exclude}}));
                    
                    $$out{$k} = $$c{$k};
                }
            }
        }
        
        say STDERR "$d: ", Dumper($out) =~ s/^\$VAR\d+ = //r;
    }
    
    return $out;
}

#     load_data($fdata)
#
# Load the JSON "database" from the JSON file $fdata. This is the standard data
# set, usually loaded from 'aiw_data.json'.
sub load_data($) {
    my $f = shift;
    
    my $data = from_json(read_text($f));
    
    # Verify the data.
    my @missing = grep { !exists $$data{$_} } (qw(expansions difficulty_sets difficulties types));

    die sprintf("Could not find keys %s in data file %s: the file might " .
                "be corrupted or incomplete. Try re-downloading it " .
                "from %s",
                join(', ', @missing),
                $f,
                'https://gitlab.com/r2b2grady/aiw-custom-randomizer/')
        if @missing;
    
    # TODO Finish validation
    
    return $data;
}

#     valid_preset($preset)
#
# Returns true if the given preset $preset is a properly formed compiled preset
# (i.e. a hashref with the key 'types' containing a list and the key 'combos'
# either non-existent or containing a list of two-element colon-separated
# lists), otherwise returns false.
sub valid_preset($) {
    my $p = shift;
    
    return ( ref($p) eq 'HASH' &&
             exists $$p{types} &&
             ref($$p{types}) eq 'ARRAY' &&
             ( !exists($$p{combos}) ||
               ( ref($$p{combos}) eq 'ARRAY' &&
                 scalar(@{$$p{combos}}) == grep { m/^[-a-z]+:[-a-z]+$/ } @{$$p{combos}}) ) );
}

#     write_preset($preset, $file)
#
# Write the compiled preset $preset to the file $file for future use.
sub write_preset($$) {
    my $p = shift;
    my $f = shift;
    
    open my $fh, '>', $f or die "Error opening $f for writing: $!";
    
    die sprintf("Preset is malformed:\n%s",
                Dumper($p) =~ s/^\$VAR\d+ = //r)
        unless valid_preset($p);
    
    say $fh to_json($p);
    
    close $fh or die "Error closing $f filehandle: $!";
}


__END__

=head1 CONFIGURATION FILES

The program uses two configuration files: C<aiw_data.json>, which stores the AI
Type, Difficulty, and Expansion data, and C<aiwrand_cfg.json>, which stores the
compiled configuration directory and a list of .

=head1 CUSTOM PRESET SCHEMA

The JSON files for custom presets must adhere to the following schema:

=over 8

=item *

The root element must be an object. Each name must be the abbreviated name of
the preset and must B<NOT> be duplicated in any of the other custom preset files
or in C<config/aiw_data.json>.

=item *

Each element of the root array must be an object with the field B<name>,
containing the human-readable name of the preset.

=item *

Each element of the root array must also have at least B<one> of the following
fields:

=over 4

=item B<exclude>

An object indicating which AI Types to exclude from consideration (see B<-e>),
with at least one of the following fields:

=over 4

=item B<difficulty>

A list of difficulty names/aliases/keys to exclude.

=item B<expansions>

A list of expansion names/keys to exclude.

=item B<types>

A list of specific AI TYpe names/keys to exclude.

=back

=item B<include>

An object indicating which AI Types to include in the list of allowable types
(see B<-i>), with the same field specification as the B<exclude> field listed
above.

=item B<pairs>

A list of combinations to exclude (see B<-x>), each of which is stored as a
two-element list. Each element of the list must be a string that matches the
B<key> or B<name> field of an AI Type from C<aiw_data.json>.

=back

=back

Multiple files can be used, a single large file with multiple configurations, or
any combination thereof. See the heading B<Example> below for a sample of a
custom JSON configuration.

=head2 Inclusion & Exclusion Priority

To keep things from getting incredibly complex, the program uses a very simple
logic for parsing inclusions and exclusions--exclusions and exclusions are
ranked (similar to mathematical operations) and then applied from lowest to
highest rank.

The ranks, from lowest to highest, are:

=over 8

=item 1

B<Difficulty-Level> Inclusions - B<Harder>, etc.

=item 2

B<Difficulty-Level> Exclusions

=item 3

B<Expansion> Inclusions - B<Children of Neinzul>, etc.

=item 4

B<Expansion> Exclusions

=item 5

B<Individual> Inclusions

=item 6

B<Individual> Exclusions

=back

That is, an excluded Difficulty Level trumps an included Difficulty Level, an
included Expansion trumps an excluded Difficulty Level, an included Individual
AI Type trumps an excluded Difficulty Level or Expansion, and an excluded
Individual AI Type trumps everything else.

To look at it another way: When the program loads a configuration, it first adds
every AI Type that is one of the difficulties listed in
C<$.include.difficulty[*]> (all JSONPaths in this example are relative to the
root of the specific configuration that was loaded)--if nothing was specified
for I<any> of the C<$.include.*> fields, then it treats it the same as if all
difficulties were loaded (i.e. C<$.include = { "difficulty": [ "a" ] }>).

The program then looks at the C<$.exclude.difficulty[*]> elements and removes
any AI Types with the specified difficulties from the list of "allowed types".

After that, the program looks at C<$.include.expansions[*]> and adds back any of
the AI Types from those expansions that were removed by the
C<$.exclude.difficulty[*]> section. Then the program looks at
C<$.exclude.expansions[*]> and removes any AI types from any of the listed
expansions that haven't already been removed.

After handling the expansions, the program looks at the C<$.include.types[*]>
elements and adds each of the specific types listed there that aren't already in
the set of types to allow.

Then, in the final preparation step, the program looks at C<$.exclude.types[*]>
and removes any of the types listed there from the list of allowed types.

=head3 Inclusion/Exclusion Example

For example, take the following configuration excerpt:

    {
      "name": "Op Order Example",
      "key": "op-example",
      "include": {
        "difficulty": [
          "a"
        ],
        "expansions": [
          "as",
          "dw",
          "vm"
        ],
        "types": [
          "golemite",
          "core",
          "tech-turtle"
        ]
      },
      "exclude": {
        "difficulty": [
          "t",
          "h"
        ],
        "expansions": [
          "zr",
          "vm"
        ],
        "types": [
          "golemite",
          "vanilla",
          "backdoor"
        ]
      }
    }

B<$.include.difficulty[*]> contains the single-letter key C<"a">, which stands
for "All Difficulties", so the program loads in every AI TYpe.

Next, C<$.exclude.difficulty[*]> contains the elements C<"t"> and C<"h">, which
represent the Technologist and Hard types, respectively. The program thus
removes all Hard/Technologist AI Types from the list of Allowed Types.

Now we look at the Expansions. C<$.include.expansions[*]> contains C<"as">,
C<"dw">, and C<"vm">--"Ancient Shadows", "Devourer of Worlds", and "Vengeance of
the Machine", respectively. So we add back all of the Harder/Technologist Types
from those expansions.

The C<$.exclude.expansions[*]> contains the values C<"zr"> and C<"vm">--"Zenith
Remnant" and "Vengeance of the Machine"--so we remove I<all> AI Types from those
two expansions. Note that this makes the C<"vm"> in C<$.include.expansions[*]>
unnecessary.

Finally, we look at the specific Types. C<$>include.types[*]> includes
C<"golemite">, C<"core">, and C<"tech-turtle">, so we add the Golemite, Core,
and Technologist Turtle back to the list of Allowed Types. C<$.exclude.types>
contains C<"golemite">, C<"vanilla">, and C<"backdoor">, so we remove the
Golemite, Vanilla, and Backdoor Hacker types from the list again--as with the
Vengeance of the Machine expansion, this renders the C<"golemite"> in
C<$.include.types[*]> pointless.

=head2 Example Custom Configuration

=head3 JSON File

    {
      "low-pain": {
        "name": "Easier-/Moderate-er",
        "include": {
          "difficulty": [
            "e",
            "m"
          ]
        },
        "exclude": {
          "types": [
            "artill",
            "turtle",
            "vanilla",
            "backdoor"
          ]
        }
      },
      "flipflop": {
        "name": "Flip-Flop"",
        "include": {
          "difficulty": [
            "e",
            "h"
          ]
        },
        "pairs": [
          [
            ["mines", "n-viral", "fort-baron"],
            ["grav-drill", "ninny"]
          ],
          [
            ["turtle", "teleturtle", "vanilla", "shadow", "scorch"],
            ["turtle", "teleturtle", "vanilla", "shadow", "scorch"]
          ],
          [
            ["sp-hammer", "shadow", "retaliatory", "jammer"],
            ["quadratic", "assassin", "alarmist", "golemite"]
          ]
        ]
      }
    }

=head3 Presets

=head4 Easier-/Moderate-er

Allows all Easier B<or> Moderate difficulty types but excludes the types
B<Turtle>, B<Vanilla>, B<Artillerist>, and B<Backdoor Hacker>.

Can be accessed with the name "Easier-/Moderate-er" or the key "low-pain".

=head4 Flip-Flop

Allows all Easier B<or> Harder difficulty types. Excludes each of the following
combinations:

=over 8

=item * 

(Grav Driller / Shield Ninny) and (Mine Enthuisiast / Neinzul Viral Enthusiast /
Fortress Baron)

=item * 

Any combination of Turtle, Teleporter Turtle, Vanilla, Shadow Master, and
Scorched Earth

=item * 

(Radar Jammer / Retaliatory / Shadow Master / Spire Hammer) and (Alarmist /
Assassin / Golemite / Quadratic)

=back

=cut
