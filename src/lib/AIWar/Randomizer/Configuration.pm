# A representation of an AI War Randomizer configuration.

package AIWar::Randomizer::Configuration;

use v5.18;

use Moose;

use File::Slurper qw(read_text);
use JSON;

=head1 AIWar::Randomizer::Configuration

A representation of the configuration for an AI War Randomizer instance.

=head1 CONSTRUCTOR

=head2 new($path)

Create a new B<AIWar::Randomizer::Configuration> instance from the JSON file
located at I<$path>.

=cut

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;
    
    my $f;
    
    if (@_ % 2) {
        $f = shift @_;
    }
    
    my %args = @_;
    
    $f = $args{-file} unless $f;
    
    die "Cannot find file $f" unless -f $f;
    
    delete $args{-file} if exists $args{-file};
    
    if (-f $f) {
        # %args = load_file($f);
        %args = load_file($f);
    }
    
    return $class->$orig(%args);
};

=head1 ATTRIBUTES

=head2 

=cut



sub load_file {
    my $f = shift;
    
    my $jcfg = decode_json(read_text($f))
        or die "Error reading JSON file $f: $!";
    
    # TODO Finish this once the attributes are all codified.
}

no Moose;
__PACKAGE__->meta->make_immutable;
